# Nuxt.js Fundamentals

Features:  
- Server Side Rendering (SSR)  
- Search Engine Optimization (SEO)  
- Meta Tags  
- Performance  

Pre Rendering:  
- The HTML files are created upfront.  
- SSR benefits + free hosting  

Code Splitting:  
- Split JS code into multiple files  
- Performance  
- Cheaper for the clients  

## CLI
```sh
npx create-nuxt-app <project-name>
or
yarn create nuxt-app <project-name>

To get started:
    cd nuxt-fundamentals
    yarn dev

To build & start for production:
    cd nuxt-fundamentals
    yarn build
    yarn start

To test:
    cd nuxt-fundamentals
    yarn test
```

## Deploy
### Heroku
https://nuxtjs.org/faq/heroku-deployment/  
https://vueschool.io/lessons/how-to-deploy-nuxtjs-to-heroku  

```
sudo systemctl enable --now snapd.socket
yay -S snapd
sudo ln -s /var/lib/snapd/snap /snap
sudo snap install --classic heroku
export PATH="$PATH:/var/lib/snapd/snap/bin"

heroku login
heroku git:remote -a nuxt-fundamentals-ab
heroku config:set NPM_CONFIG_PRODUCTION=false
heroku config:set HOST=0.0.0.0
heroku config:set NODE_ENV=production
add  `"heroku-postbuild": "npm run build"` to the end of the scripts section in package.json
git push heroku master
```

### Netlify
```
# Generate static pages
yarn generate
```