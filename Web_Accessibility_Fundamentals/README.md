# Web Accessibility Fundamentals
Course: https://vueschool.io/courses/web-accessibility-fundamentals  

## Prerequisites
- Using Google Chrome with [Vue devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/ljjemllljcmogpfapbkkighbhhppjdbg) installed.  
- Using Visual Studio Code with [Vetur (Vue extension)](https://marketplace.visualstudio.com/items?itemName=octref.vetur) installed.  

## What we cover?
- Web content accessibility guidelines
- Accessible design
- Good content structure
- Accessibility testing with Chrome dev tools
- Assistive technologies: [Web Accessibility Initiative – Accessible Rich Internet Applications (WAI-ARIA)](https://en.wikipedia.org/wiki/WAI-ARIA)

## Notes  
- Accessibility = A11y  

### Population
- According to the World Health Organization:
    - 1.3 billion people worldwide have a disability
    - 15% of the world population
    - 1 in 7 people
- People with disabilities are the world's largest minority
- People with disabilities and their family and friends control over 8 trillion dollars in annual disposable income (a number companies can't ignore)
- Users can experience varying degrees of disability: oratory, cognitive, physical, speech, visual.
- You can improve the experience of a lot of users by making sure your applications are: 
    - Accessible to screen readers  
    - Tabbable (without the use of a mouse)  
    - Meeting color contrast criteria  
- Improve experience for all users:
    - Having low bandwidth connections
    - Using older technologies
    - Are in a noisy environment
    - Mobile phone users
    - Brand new users
    - Language barriers
    - Older people
    - Temporary disabilities
- [Demo app with simulated visual impairment](https://visual-simulation.netlify.com/)
- Voice over tools
    - Mac: hit cmd + f5 to enable the built-in VoiceOver tool
    - Windows: download [NVDA VoiceOver tool](https://www.nvaccess.org/download/)  

### Business advantages of having accessible websites
1. Drive innovation  
Assistive technology.  

2. Enhance your brand  
Loyal brand followers.  

3. Extend market reach  
1.3 billion people with disabilities worldwide.  

4. Minimize legal risk  
Requirement for all government websites.  
