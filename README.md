# vue-school

Learn Vue.js via https://vueschool.io/

## Beginner
- [x] [Vue.js Fundamentals](https://vueschool.io/courses/vuejs-fundamentals)
- [x] [Vue.js Components Fundamentals](https://vueschool.io/courses/vuejs-components-fundamentals)
- [ ] [Vue.js + Firebase Realtime Database](https://vueschool.io/courses/vuejs-firebase-realtime-database)
- [ ] [Vue.js + Firebase Authentication](https://vueschool.io/courses/vuejs-firebase-authentication)

## Intermediate
- [ ] [Single File Components](https://vueschool.io/courses/vuejs-single-file-components)
- [ ] [Vue Router For Everyone](https://vueschool.io/courses/vue-router-for-everyone)
- [ ] [Vue.js Form Validation](https://vueschool.io/courses/vuejs-form-validation)
- [ ] [Internationalization with vue-i18n](https://vueschool.io/courses/internationalization-with-vue-i18n)
- [ ] [The Vue.js Master Class](https://vueschool.io/courses/the-vuejs-master-class)

## Advanced
- [ ] [Vuex for Everyone](https://vueschool.io/courses/vuex-for-everyone)
- [ ] [Dynamic Forms with Vue.js](https://vueschool.io/courses/dynamic-forms-vuejs)
- [ ] [Custom Vue Directives](https://vueschool.io/courses/custom-vuejs-directives)

## JavaScript
- [x] [Web Accessibility Fundamentals](https://vueschool.io/courses/web-accessibility-fundamentals)
- [ ] [Modern JavaScript: ES6 and beyond!](https://vueschool.io/courses/modern-javascript-es6-and-beyond)

## Testing
- [x] [JavaScript Testing Fundamentals](https://vueschool.io/courses/javascript-testing-fundamentals)
- [ ] [Test with Jest](https://vueschool.io/courses/test-with-jest)
- [ ] [Testing Vue.js Components](https://vueschool.io/courses/learn-how-to-test-vuejs-components)

## Nuxt.js
- [x] [Nuxt.js Fundamentals](https://vueschool.io/courses/nuxtjs-fundamentals)
- [ ] [Async Data with Nuxt.js](https://vueschool.io/courses/async-data-with-nuxtjs)
- [ ] [Static Site Generation with Nuxt.js](https://vueschool.io/courses/static-site-generation-with-nuxtjs)
