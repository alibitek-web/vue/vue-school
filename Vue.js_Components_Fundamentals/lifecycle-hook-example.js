let BlogPostComponent = {
  props: ['id'],
  data () {
      return {
        blogPost: null,
      };
  },
  created () { 
    // https://github.com/axios/axios
    // or
    // https://github.com/github/fetch
    // https://github.com/taylorhakes/promise-polyfill
    // https://blog.logrocket.com/axios-or-fetch-api/
    axios.get('api/posts/' + this.id).then(response => {
        this.blogPost = response.data;
    });
  },
};
