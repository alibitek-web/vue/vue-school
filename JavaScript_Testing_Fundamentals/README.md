# JavaScript Testing Fundamentals
Course: https://vueschool.io/courses/javascript-testing-fundamentals

## Notes

### Why is testing important?
1. Confidence in your code  
Ensure everything works the way that it should.  

2. Documentation for developers  

3. Refactoring gets easy  

4. Become a better developer  

5. Working in a team  

### What to test?
What does it actually make sense to test?  
Small, important pieces of the code, related to the user flow.  
Around 80% test coverage is healthy.  

### Types of testing
- Unit tests  
- Integration tests  
- Acceptance tests (often called end-to-end tests)  

![Types of tests](./Types_of_tests.png "Types of tests")

![Example unit test](./Unit_test_example.png "Example unit test")

![Example acceptance test](Acceptance_test_example.png "Example acceptance test")

### What are mocks and mocking?
Replace entire dependency with a mock (imitation, dummy clone).  
A mock is an alternate implementation of a dependency when testing.  

![Mocking](./Mocking.png "Mocking")

### What are stubs?
Stubs are similar to mocks, but they only replace small parts of your code compared to mocks who replace the entire dependency.  

![Stubs](./Stubs.png "Stubs")

### What are spies?
You can see spies as watchers for functions and events.  
You can assert if they are called, how many times there were called, with which parameters they were called and what they return.  
Helpful for async actions like callbacks or events.  

![Spies](./Spies.png "Spies")

### Vue testing tools
- `vue-test-utils`
    - Utilities for testing Vue components
    - https://github.com/vuejs/vue-test-utils  
    - https://vue-test-utils.vuejs.org/  
    - Examples:
        - https://github.com/vuejs/vue-test-utils-jest-example
        - https://github.com/vuejs/vue-test-utils-getting-started
        - https://github.com/vuejs/vue-test-utils-typescript-example
- `vue-jest` 
    - Preprocessor that compiles the script and template of SFCs into a JavaScript file that Jest can run
    - https://github.com/vuejs/vue-jest

### Jest test runner
- https://en.wikipedia.org/wiki/Jest_(JavaScript_framework)
- https://jestjs.io/
- https://jestjs.io/docs/en/getting-started
- https://github.com/facebook/jest

```sh
$ yarn add --dev jest
$ cd sum_example
$ yarn test
yarn run v1.21.1
$ jest
 PASS  ./sum.test.js
  ✓ adds 1 + 2 to equal 3 (2ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        0.778s
Ran all test suites.
Done in 1.28s.
```